from django.test import TestCase, Client
from .views import index
from django.apps import apps
from .apps import AccordionConfig
from django.urls import resolve

# Create your tests here.
class UnitTestStory7(TestCase):
    def test_apps(self):
        self.assertEqual(AccordionConfig.name, 'accordion')
        self.assertEqual(apps.get_app_config('accordion').name, 'accordion') 

    def test_url_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_get(self):
            response = Client().get('/')
            html_kembalian = response.content.decode('utf8')
            self.assertIn("hello Accordion", html_kembalian)
            self.assertIn("About Me", html_kembalian)
            self.assertIn("Capability and Skills", html_kembalian)   
            self.assertIn("Personal Experiences", html_kembalian)
            self.assertIn("All Times Favorite Games", html_kembalian)


